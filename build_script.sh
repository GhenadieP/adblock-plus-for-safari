#!/bin/sh
build_start_time=$(date "+%Y.%m.%d-%H.%M.%S")

#------------------- Specific Builds --------------- #

appBuild() {
    build
    archive
    exportApp
}

#---------------------- Base functions -------------------#

build() {
     set -o pipefail && xcodebuild clean build -project AdblockPlusSafari.xcodeproj \
                        -scheme "AdblockPlusSafari" -sdk iphoneos -destination "generic/platform=iOS" \
                         -allowProvisioningUpdates | xcpretty -s
 }

 archive() {
     xcodebuild -project AdblockPlusSafari.xcodeproj -scheme "AdblockPlusSafari" -archivePath build/artifacts/AdblockPlusSafari.xcarchive archive | xcpretty -s
 }

 exportApp() {
     xcodebuild -exportArchive -archivePath build/artifacts/AdblockPlusSafari.xcarchive -exportPath build/artifacts -exportOptionsPlist ExportOptions.plist | xcpretty -s
 }

# ---------------- Main -----------------#
appBuild




