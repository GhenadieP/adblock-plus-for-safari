/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import SafariServices
import UIKit

class SettingsViewController: UITableViewController {

    @IBOutlet weak var analyticsSwitch: UISwitch!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    @IBOutlet weak var appVersionNumberLabel: UILabel!

    @IBAction func analyticsSwitchValueDidChange(_ sender: UISwitch) {
        let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)
        groupDefaults?.set(sender.isOn, forKey: Constants.analyticsConsent)
        switch sender.isOn {
        case true:
            FirebaseWrapper.initializeFirebase()
            FabricWrapper.initializeFabric()
            FirebaseWrapper.sendEvent(event: AnalyticEvents.settingsConsentSwitchActive)
        case false:
            FirebaseWrapper.sendEvent(event: AnalyticEvents.settingsConsentSwitchInactive)
            FirebaseWrapper.deleteFirebase()
        }
    }

    @IBAction func didPressPrivacyPolicyButton(_ sender: Any) {
        FirebaseWrapper.sendEvent(event: AnalyticEvents.settingsPrivacyPolicyViewed)
        guard let url = URL(string: Constants.privacyPolicyURL) else { return }
        openWebpage(url)
    }

    private func openWebpage(_ url: URL) {
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        let safariViewController = SFSafariViewController(url: url, configuration: config)
        safariViewController.preferredControlTintColor = UIColor.customColor(.abpRed)
        present(safariViewController, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        appVersionNumberLabel.text = Constants.currentVersionNumber
        analyticsSwitch.isOn = FirebaseWrapper.analyticsConsentIsGiven()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        FirebaseWrapper.sendEvent(event: AnalyticEvents.settingsShown)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        FirebaseWrapper.sendEvent(event: AnalyticEvents.settingsDismissed)
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
