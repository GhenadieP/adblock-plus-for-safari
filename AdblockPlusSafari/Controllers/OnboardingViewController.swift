/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import AVKit
import Lottie
import Markup
import UIKit

class OnboardingViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet var animationView: AnimationView!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet weak var analConsentSwitch: UISwitch!
    // Root StackView Outlets
    @IBOutlet var rootStackView: UIStackView!
    @IBOutlet var subtitleLabel: UILabel!
    // Title StackView Outlets
    @IBOutlet var titleStackView: UIStackView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var watchVideoButton: UIButton!
    @IBOutlet var titleSpacer: UIView!
    // Body StackView Outlets
    @IBOutlet var bodyStackView: UIStackView!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet var bodySpacer: UIView!
    // Instructions Screen Outlets
    @IBOutlet var instructionsStackView: UIStackView!
    @IBOutlet weak var instructionSpacer2: UIView!
    @IBOutlet weak var instructionLabel2: UILabel!
    @IBOutlet weak var instructionLabel3: UILabel!
    @IBOutlet weak var instructionLabel4: UILabel!
    @IBOutlet weak var instructionSubStackView3: UIStackView!
    // Autolayout Constraints
    @IBOutlet weak var animationViewRegularAspectConstraint: NSLayoutConstraint!
    @IBOutlet weak var animationViewNarrowAspectConstraint: NSLayoutConstraint!
    // Instance Varibles
    private let videoViewController = AVPlayerViewController()
    // swiftlint:disable:next discouraged_direct_init
    private let userInterfaceIdiom = UIDevice().userInterfaceIdiom

    // MARK: - IBActions

    @IBAction func nextButtonPressed(_ sender: UIButton) {
        // Set analytics consent
        let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)
        groupDefaults?.set(analConsentSwitch.isOn, forKey: Constants.analyticsConsent)
        if !FirebaseWrapper.firebaseIsInitialized() {
            FirebaseWrapper.initializeFirebase()
        }
        // Get CB state and skip onboarding instructions if already enabled.
        let contentBlockerState = ContentBlockerManager.shared().getContentBlockerState()
        switch contentBlockerState {
        case true:
            // Skip onboarding instructions
            FirebaseWrapper.sendEvent(event: AnalyticEvents.obNextButtonCBActive)
            self.dismiss(animated: true) {
            }
        case false:
            // Continue to onboarding instructions
            FirebaseWrapper.sendEvent(event: AnalyticEvents.obInstructionsShown)
            animationView.play()
            nextButtonAnimations()
        }
    }

    @IBAction func watchVideoPressed(_ sender: Any) {
        FirebaseWrapper.sendEvent(event: AnalyticEvents.obWatchVideoStart)
        playOnboardingVideo()
    }

    // MARK: - View Logic

    override func viewDidLoad() {
        super.viewDidLoad()
        viewLoadAnimation()
        applyStyles()
        setupNotifications()
        applyAttributedText()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        applyConditionalConstraints()
    }

    // MARK: - Custom Styles

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    fileprivate func applyAttributedText() {
        // Markup Renderer
        let instructionRenderer = MarkupRenderer(baseFont: .systemFont(ofSize: 14))

        // Instruction step loaclised texts.
        let instructionStep2Phone = "Open the *iPhone Settings* app.".localized
        let instructionStep2Pad = "Open the *iPad Settings* app.".localized
        let instructionStep3 = "Locate and tap *Safari*, then tap *Content Blockers*.".localized
        let instructionStep4 = "Toggle on *Adblock Plus*.".localized

        // Apply attributed strings.
        // Device specific adjustments.
        switch userInterfaceIdiom {
        case .phone:
            instructionLabel2.attributedText = instructionRenderer.render(text: instructionStep2Phone)
        default:
            instructionLabel2.attributedText = instructionRenderer.render(text: instructionStep2Pad)
        }
        instructionLabel3.attributedText = instructionRenderer.render(text: instructionStep3)
        instructionLabel4.attributedText = instructionRenderer.render(text: instructionStep4)
    }

    /// Apply custom styles to UI elements.
    fileprivate func applyStyles() {

        // Set custom element spacing in root stack view.
        rootStackView.setCustomSpacing(16, after: titleStackView)
        rootStackView.setCustomSpacing(16, after: titleSpacer)

        // Set custom element spacing in instructions stack view.
        instructionsStackView.setCustomSpacing(8, after: instructionSpacer2)
        instructionsStackView.setCustomSpacing(8, after: instructionSubStackView3)

        // Apply button border style.
        watchVideoButton.layer.borderColor = UIColor.customColor(.abpRed)?.cgColor

        // Device specific adjustments.
        if  userInterfaceIdiom == .phone {
            let iPhone5Height = 568
            if Int(UIScreen.main.bounds.size.height) == iPhone5Height {
                // iPhone 5/SE Adjustments
                subtitleLabel.isHidden = true
                titleSpacer.isHidden = true
                bodyStackView.setCustomSpacing(8, after: bodySpacer)
                bodyStackView.setCustomSpacing(8, after: bodyLabel)
            }
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        applyConditionalConstraints()
    }

    /// Applys custom autolayout constraints depending on device orintation.
    fileprivate func applyConditionalConstraints() {
        switch UIDevice.current.orientation.isLandscape {
        case true:
            animationViewRegularAspectConstraint.isActive = false
            animationViewNarrowAspectConstraint.isActive = true
        case false:
            animationViewRegularAspectConstraint.isActive = true
            animationViewNarrowAspectConstraint.isActive = false
        }
    }

    // MARK: - Animation Logic

    /// Animations to play when view first loads.
    fileprivate func viewLoadAnimation() {
        // Header animation
        let animation = Animation.named("onboardingAnimation")
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFill
        animationView.play(toFrame: 151)

        rootStackView.alpha = 0
        rootStackView.layer.position = CGPoint(x: 0, y: 32)
        UIView.animate(withDuration: 2.32, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.rootStackView.alpha = 1
            self.rootStackView.layer.position = CGPoint(x: 0, y: 0)
        })
    }

    /// Animations to play when the "Next" button is pressed.
    fileprivate func nextButtonAnimations() {

        let nextButtonPosition = nextButton.layer.position
        let rootStackViewPosition = rootStackView.layer.position

        let animations1 = {
            self.rootStackView.alpha = 0
            self.nextButton.alpha = 0
            self.nextButton.layer.position = CGPoint(x: nextButtonPosition.x - 32, y: nextButtonPosition.y)
            self.rootStackView.layer.position = CGPoint(x: rootStackViewPosition.x - 32, y: rootStackViewPosition.y)
        }
        let animations2 = {
            self.rootStackView.alpha = 1
            self.rootStackView.layer.position = CGPoint(x: rootStackViewPosition.x - 32, y: rootStackViewPosition.y)
        }

        UIView.animate(withDuration: 0.75, delay: 0, options: .curveEaseIn, animations: animations1, completion: { _ in
            self.subtitleLabel.text = "SIMPLE SETUP".localized
            self.titleLabel.text = "Activate\nAdblock Plus".localized
            self.bodyStackView.isHidden = true
            self.nextButton.isHidden = true
            self.watchVideoButton.isHidden = false
            self.instructionsStackView.isHidden = false
            self.rootStackView.layer.position = CGPoint(x: rootStackViewPosition.x + 32, y: rootStackViewPosition.y)
            self.rootStackView.setCustomSpacing(32, after: self.titleSpacer)

            UIView.animate(withDuration: 1.75, delay: 0, options: .curveEaseOut, animations: animations2)
        })

        UIView.animate(withDuration: 0.75, delay: 2.5, options: [.curveEaseInOut], animations: {
            self.watchVideoButton.alpha = 1
        })
    }

    // MARK: - Onboarding Video Logic

    /// Plays the onboarding video
    fileprivate func playOnboardingVideo() {
        let videoURL = Bundle.main.url(forResource: "onboarding", withExtension: "MP4")!
        let videoPlayer = AVPlayer(url: videoURL)
        videoViewController.player = videoPlayer

        // Setup observer to detect when video is finished playing
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissOnboardingVideo),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil)
        present(videoViewController, animated: true) {
            self.videoViewController.player?.play()
        }
    }

    /// Dismisses onboarding video
    @objc
    fileprivate func dismissOnboardingVideo(notification: NSNotification) {
        FirebaseWrapper.sendEvent(event: AnalyticEvents.obWatchVideoEnd)
        self.videoViewController.dismiss(animated: true) {}
    }

    // MARK: - App Notifications

    fileprivate func setupNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(appWillEnterForeground),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(appDidEnterBackground),
                                       name: UIApplication.didEnterBackgroundNotification,
                                       object: nil)
    }

    @objc
    func appWillEnterForeground() {
        FirebaseWrapper.sendEvent(event: AnalyticEvents.obInstructionsForeground)
        let contentBlockerState = ContentBlockerManager.shared().getContentBlockerState()
        if contentBlockerState == true {
            FirebaseWrapper.sendEvent(event: AnalyticEvents.obInstructionsDismissed)
            self.dismiss(animated: true) {
            }
        }
    }

    @objc
    func appDidEnterBackground() {
        FirebaseWrapper.sendEvent(event: AnalyticEvents.obInstructionsBackground)
    }

    deinit {
       NotificationCenter.default.removeObserver(self)
    }
}
