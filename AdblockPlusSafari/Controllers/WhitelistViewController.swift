/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit

class WhitelistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var whitelistArray: [String] = []
    let whitelistDataSource = WhitelistItemDataSource()
    // swiftlint:disable:next discouraged_direct_init
    private let userInterfaceIdiom = UIDevice().userInterfaceIdiom

    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var whitelistActivityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyles()
        setupNotifications()
        whitelistDataSource.getWhitelistArray { list in
            self.whitelistArray = list
            self.tableView.reloadData()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ContentBlockerManager.shared().delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        FirebaseWrapper.sendEvent(event: AnalyticEvents.whitelistShown)
        toggleWhitelistInstructions()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        FirebaseWrapper.sendEvent(event: AnalyticEvents.whitelistDismissed)
    }

    /// Disables UIButtons
    /// - Parameter state: Passing true will enable loading state or false will disable the loading state.
    fileprivate func setLoadingState(asEnabled state: Bool) {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.isUserInteractionEnabled = !state
            self.tableView.allowsSelection = !state
            self.tableView.isUserInteractionEnabled = !state
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = !state
            switch state {
            case true:
                self.navigationController?.navigationBar.alpha = 0.5
                self.tableView.alpha = 0.5
            default:
                self.navigationController?.navigationBar.alpha = 1
                self.tableView.alpha = 1
            }
        }
    }

    /// Starts or stops spinner
    /// - Parameter state: Passing true will enable spinner or false will disable the spinner.
    fileprivate func setSpinnerState(asEnabled state: Bool) {
        DispatchQueue.main.async {
            switch state {
            case true:
                self.whitelistActivityIndicator.startAnimating()
            default:
                self.whitelistActivityIndicator.stopAnimating()
            }
        }
    }

    /// Will show or hide whitelist instructions if whitelist array is empty or not, and trigger update to whitelist tableview cells.
    fileprivate func toggleWhitelistInstructions() {
        whitelistDataSource.getWhitelistArray { [weak self] whitelistArray in
            self?.tableView.isHidden = whitelistArray.isEmpty
            self?.addView.isHidden = !whitelistArray.isEmpty
            self?.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return whitelistArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WhitelistCell", for: indexPath)
        cell.textLabel?.text = whitelistArray[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.setLoadingState(asEnabled: true)

        let alertController = UIAlertController(title: whitelistArray[indexPath.row], message: nil, preferredStyle: .actionSheet)

        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
            FirebaseWrapper.sendEvent(event: AnalyticEvents.whitelistRemoveFromWhitelist)
            self.removeWhitelistItem(at: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.setLoadingState(asEnabled: false)
        }

        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)

        // If device is an iPad, this will add a cancel button to the popup, as by
        // default the .cancel style is hidden on iPad
        if userInterfaceIdiom == .pad {
            let cancelActionPad = UIAlertAction(title: "Cancel", style: .default) { _ in
                self.setLoadingState(asEnabled: false)
            }
            alertController.addAction(cancelActionPad)
        }

        // If device requires a popoverPresentationController (iPad) then this
        // will initialise to show in the middle of the screen.
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }

        present(alertController, animated: true, completion: nil)

        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { _, indexPath in
            self.setLoadingState(asEnabled: true)
            FirebaseWrapper.sendEvent(event: AnalyticEvents.whitelistRemoveFromWhitelist)
            self.removeWhitelistItem(at: indexPath)
        }

        return [delete]
    }

    fileprivate func removeWhitelistItem(at indexPath: IndexPath) {
        // Get URL from senders cell
        guard let url = tableView.cellForRow(at: indexPath)?.textLabel?.text else {
            setLoadingState(asEnabled: false)
            return
        }

        // Remove URL from array
        self.whitelistDataSource.removeURLfromArray(url: url) { domains in
            self.whitelistArray = domains
            self.tableView.reloadData()
            // Update UI
            self.toggleWhitelistInstructions()
            self.setSpinnerState(asEnabled: true)

            // Reload Content Blocker
            ContentBlockerManager.shared().reloadContentBlocker()
        }
    }

    @objc
    func didPressAddButton(_ sender: Any) {
        self.setLoadingState(asEnabled: true)
        let alertController = UIAlertController(title: "Add website".localized,
                                                message: "Type in a valid URL to add any website to your whitelist.".localized,
                                                preferredStyle: .alert)

        alertController.addTextField { textField in
            textField.placeholder = "Website URL".localized
            textField.isSecureTextEntry = false
            textField.keyboardType = .URL
        }

        let addAction = UIAlertAction(title: "Add".localized, style: .default) { _ in
            FirebaseWrapper.sendEvent(event: AnalyticEvents.whitelistAddToWhitelist)

            // Unwraps and checks input string is not empty, can be formatted to a valid hostname and is not a duplicate
            guard let alertText = alertController.textFields?.first?.text,
                !alertText.isEmpty,
                let hostname = NSString(string: alertText).whitelistedHostname(),
                !self.whitelistArray.contains(hostname) else {
                    self.setLoadingState(asEnabled: false)
                    return
            }

            // Add Hostname to WhitelistArray
            self.whitelistDataSource.appendToWhitelist(hostname: hostname) { domains in
                self.whitelistArray = domains

                // Update UI
                self.toggleWhitelistInstructions()
                self.setSpinnerState(asEnabled: true)
                self.tableView.reloadData()

                // Reload Content Blocker
                ContentBlockerManager.shared().reloadContentBlocker()
            }
        }

        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { _ in
            self.setLoadingState(asEnabled: false)
        }

        alertController.addAction(cancelAction)
        alertController.addAction(addAction)

        present(alertController, animated: true, completion: nil)
    }

    // Apply custom colors when app switches between dark mode.
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        addView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
    }

    /// Apply custom styles to UI elements.
    private func applyStyles() {
        addView.layer.cornerRadius = 8
        addView.layer.borderWidth = 1
        addView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor

        // Add button for navigation bar.
        let addButton = UIButton(type: .system)
        addButton.frame = CGRect(x: 0, y: 0, width: 42, height: 24)
        addButton.layer.masksToBounds = false
        addButton.layer.cornerRadius = 4
        addButton.layer.borderWidth = 1
        addButton.layer.borderColor = UIColor.customColor(.abpRed)?.cgColor
        addButton.setTitle("ADD".localized, for: .normal)
        addButton.setTitleColor(UIColor.customColor(.abpRed), for: .normal)
        addButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        addButton.addTarget(self, action: #selector(didPressAddButton(_:)), for: .touchUpInside)

        let barButtonItemAdd = UIBarButtonItem(customView: addButton)
        self.navigationItem.rightBarButtonItem = barButtonItemAdd
    }

    fileprivate func setupNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(appWillEnterForeground),
                                       name: UIApplication.willEnterForegroundNotification,
                                       object: nil)
    }

    @objc
    func appWillEnterForeground() {
        toggleWhitelistInstructions()
    }

    deinit {
       NotificationCenter.default.removeObserver(self)
    }
}

extension WhitelistViewController: ContentBlockerManagerDelegate {
    func contentBlockerStateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
        setSpinnerState(asEnabled: false)
    }

    func contentBlockerUpdateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
        setSpinnerState(asEnabled: false)
    }

    func contentBlockerUpdateDidSucceed() {
        setLoadingState(asEnabled: false)
        setSpinnerState(asEnabled: false)
    }
}
