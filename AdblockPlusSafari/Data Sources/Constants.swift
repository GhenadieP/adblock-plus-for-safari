/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

struct Constants {
    // App Bundle Identifiers
    static let appIdentifier = "org.adblockplus.AdblockPlusSafari"
    static let contentBlockerIdentifier = "org.adblockplus.AdblockPlusSafari.AdblockPlusSafariExtension"
    static let actionExtensionIdentifier = "org.adblockplus.AdblockPlusSafari.AdblockPlusSafariActionExtension"
    static let groupIdentifier = "group.org.adblockplus.AdblockPlusSafari"

    // Easylist Filename Variants
    static let easylist = "easylist_content_blocker"
    static let easylistExceptionRules = "easylist+exceptionrules_content_blocker"
    static let easylistTestRules = "abp-testcase-subscription"

    // Shared Filterlist Filepaths
    static let sharedFilterlist = "sharedFilterlist"
    static let sharedFilterlistOriginal = "sharedFilterlist_original"
    static let sharedFilterlistPlusExceptionRules = "sharedFilterlist+exceptionrules"
    // swiftlint:disable:next identifier_name
    static let sharedFilterlistPlusExceptionRulesOriginal = "sharedFilterlist+exceptionrules_original"
    static let sharedFilterlistTest = "sharedFilterlistTest"

    // User Defaults Keys
    /// Returns a Bool if app has previously run
    static let hasRun = "hasRun"
    /// Returns a string of the stored app version, set when app last run
    static let storedAppVersion = "installedAppVersion"
    /// Returns a Bool to see if onboarding should be shown
    static let showOnboarding = "showOnboarding"
    /// Returns a Bool to see if Block Ads is enabled
    static let blockAdsEnabled = "blockAdsEnabled"
    /// Returns a Bool to see if Acceptable Ads is enabled
    static let acceptableAdsEnabled = "acceptableAdsEnabled"
    /// Returns an Array[String] of domains to be whitelisted
    static let whitelistArray = "whitelistArray"
    /// Returns a Bool of the dismissedState of the message card
    static let messageCardDismissed = "messageCardDismissed"
    /// Returns a Bool of the consent to use analytics
    static let analyticsConsent = "analyticsConsent"

    // Legacy User Defaults Keys (from ABP 1.x)
    /// Returns a Bool to see if Block Ads is enabled
    static let legacyBlockAdsEnabled = "AdblockPlusEnabled"
    /// Returns a Bool to see if Acceptable Ads is enabled
    static let legacyAcceptableAdsEnabled = "AdblockPlusAcceptableAdsEnabled"
    /// Returns an Array[String] of domains to be whitelisted
    static let legacyWhitelistArray = "AdblockPlusWhitelistedWebsites"

    // Storyboard Identifiers
    static let OnboardingVC = "Onboarding"

    // URLs
    /// Privacy Policy URL
    static let privacyPolicyURL = "https://adblockplus.org/privacy"

    // App Version Number
    /// Returns a string of the version & build of the App from info.plist
    static var currentVersionNumber: String? {
        guard
            let appVersionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String,
            let appBuildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
            else {
                return "info.plist Read Error"
        }
        let versionBuildCombined = "\(appVersionNumber) (\(appBuildNumber))"
        return versionBuildCombined
    }
}

// Firebase Events
struct AnalyticEvents {
    // Onboarding Next Button Events
    static let obNextButtonCBActive = "OB_Next_Button_CB_Active"

    // Onboarding Instruction View Events
    static let obInstructionsShown = "OB_Instructions_Shown"
    static let obInstructionsDismissed = "OB_Instructions_Dismissed"
    static let obInstructionsBackground = "OB_Instructions_Background"
    static let obInstructionsForeground = "OB_Instructions_Foreground"

    // Onboarding Video Events
    static let obWatchVideoStart = "OB_Watch_Video_Start"
    static let obWatchVideoEnd = "OB_Watch_Video_End"

    // Main View Events
    static let mainShown = "Main_Shown"
    static let mainDismissed = "Main_Dismissed"
    static let mainBlockAdsSwitchActive = "Main_BlockAds_Switch_Active"
    static let mainBlockAdsSwitchInactive = "Main_BlockAds_Switch_Inactive"

    // Settings View Events
    static let settingsShown = "Settings_Shown"
    static let settingsDismissed = "Settings_Dismissed"
    static let settingsConsentSwitchActive = "Settings_Consent_Switch_Active"
    static let settingsConsentSwitchInactive = "Settings_Consent_Switch_Inactive"
    static let settingsPrivacyPolicyViewed = "Settings_Privacy_Policy_Viewed"

    // Exceptions View Events
    static let exceptionsShown = "Exceptions_Shown"
    static let exceptionsDismissed = "Exceptions_Dismissed"
    static let exceptionsAASwitchActive = "Exceptions_AA_Switch_Active"
    static let exceptionsAASwitchInactive = "Exceptions_AA_Switch_Inactive"

    // Whitelist View Events
    static let whitelistShown = "Whitelist_Shown"
    static let whitelistDismissed = "Whitelist_Dismissed"
    static let whitelistAddToWhitelist = "Whitelist_Add_To_Whitelist"
    static let whitelistRemoveFromWhitelist = "Whitelist_Remove_From_Whitelist"
}
