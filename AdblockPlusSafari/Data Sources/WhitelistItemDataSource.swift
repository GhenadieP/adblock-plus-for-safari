/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import ABPKit

/// Datasource for whitelist NSCollectionView
final class WhitelistItemDataSource {
    private let whitelistedDomainsLoader = WhiteListingComposer.whiteListedDomainsLoader
    private let whitelistedDomainsRemover = WhiteListingComposer.whiteListedDomainRemover
    private let whiteListedDomainSaver = WhiteListingComposer.whiteListedDomainSaver

    // MARK: - Public API
    func getWhitelistArray(completion: @escaping ([String]) -> Void) {
        whitelistedDomainsLoader.loadDomains(completion: handleCompletion(forwardTo: completion))
    }

    func appendToWhitelist(hostname: String, completion: @escaping ([String]) -> Void) {
        whiteListedDomainSaver.saveDomain(hostname,
                                          completion: handleCompletion(forwardTo: completion))
    }

    func removeURLfromArray(url: String, completion: @escaping ([String]) -> Void) {
        whitelistedDomainsRemover.removeDomain(url, completion: handleCompletion(forwardTo: completion))
    }

    // MARK: - Private methods
    private func handleCompletion(forwardTo forwardCompletion: @escaping ([String]) -> Void) -> (Result<WhitelistedWebsites, Error>) -> Void {
        return { result in
            DispatchQueue.main.async {
                forwardCompletion((try? result.get()) ?? [])
            }
        }
    }
}
