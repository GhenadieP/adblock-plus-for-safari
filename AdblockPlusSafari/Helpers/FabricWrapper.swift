/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

#if canImport(Crashlytics)
import Crashlytics
#endif
#if canImport(Fabric)
import Fabric
#endif

/// A set of helper functions to interact with Fabric.
class FabricWrapper {

    /// Checks if user consent is given and Fabric is configured, then initializes Fabric.
    class func initializeFabric() {
//        #if canImport(Fabric)
//        guard analyticsConsentIsGiven() && fabricKeyIsPresent() else {
//            print("🔥: Fabric not initialized")
//            print("🔥: Fabric Consent Given: \(analyticsConsentIsGiven())")
//            print("🔥: Fabric API Key Present: \(fabricKeyIsPresent())")
//            return
//        }
//        UserDefaults.standard.register(defaults: ["NSApplicationCrashOnExceptions": true])
//        Fabric.with([Crashlytics.self()])
//        #endif
    }

    /// - Returns: A bool confirming if the Fabric API key is present in Info.plist
    private class func fabricKeyIsPresent() -> Bool {
        guard let fabricDict = Bundle.main.object(forInfoDictionaryKey: "Fabric") as? [String: AnyObject] else { return false }
        return fabricDict["APIKey"] != nil
    }

    /// - Returns: A bool confirming if the user consents to Firebase usage.
    private class func analyticsConsentIsGiven() -> Bool {
        let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)
        let analyticsConsent = groupDefaults?.bool(forKey: Constants.analyticsConsent)
        return analyticsConsent ?? false
    }
}
