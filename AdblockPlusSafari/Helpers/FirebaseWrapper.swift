/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

#if canImport(Firebase)
import Firebase
#endif

/// A set of helper functions to interact with Firebase.
class FirebaseWrapper {

    /// - Returns: A bool confirming if the user consents to Firebase usage.
    class func analyticsConsentIsGiven() -> Bool {
        let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)
        let analyticsConsent = groupDefaults?.bool(forKey: Constants.analyticsConsent)
        return analyticsConsent ?? false
    }

    /// Checks if user consent is given and Firebase is configured, then initializes Firebase.
    class func initializeFirebase() {
//        guard analyticsConsentIsGiven() && firebaseKeyIsPresent() else {
//            print("🔥: Firebase not initialized")
//            print("🔥: Firebase Consent Given: \(analyticsConsentIsGiven())")
//            print("🔥: Firebase API Key Present: \(firebaseKeyIsPresent())")
//            return
//        }
//        #if canImport(Firebase)
//        FirebaseApp.configure()
//        Analytics.setAnalyticsCollectionEnabled(true)
//        #endif
    }

    /// Deletes the loaded instance of Firebase.
    class func deleteFirebase() {
//        #if canImport(Firebase)
//        Analytics.setAnalyticsCollectionEnabled(false)
//        let firebaseApp = FirebaseApp.app()
//        firebaseApp?.delete({ deleted in
//            print("🔥: Firebase instance deleted: \(deleted)")
//        })
//        #endif
    }

    /// - Returns: A bool confirming if the Firebase API key is present in GoogleService-Info.plist
    class private func firebaseKeyIsPresent() -> Bool {

        if let path = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist") {
            let nsDictionary = NSDictionary(contentsOfFile: path) as? [String: AnyObject]
            let apiKey = nsDictionary!["API_KEY"]
            return !(apiKey?.isEqual(to: "correct_api_key"))!
        }
        return false
    }

    /// Sends a custom event to Firebase if consent is given and firebase is configured.
    /// - Parameter event: A string describing the event name.
    class func sendEvent(event: String) {
//        guard analyticsConsentIsGiven() && firebaseKeyIsPresent() else { return }
//        Analytics.logEvent(event, parameters: nil)
    }

    /// - Returns: A bool of the initialized state of Firebase.
    class func firebaseIsInitialized() -> Bool {
        guard FirebaseApp.app() != nil else {
            return false
        }
        return true
    }
}
