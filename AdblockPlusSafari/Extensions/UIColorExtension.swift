/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit

enum CustomColor {
    case abpAlmostWhite
    case abpBackgroundGray
    case abpDarkGray
    case abpLightGray
    case abpPureWhite
    case abpPureWhiteTrans
    case abpRed
}

extension UIColor {
    /// - Returns: A named UI color from the asset catalog.
    static func customColor(_ color: CustomColor) -> UIColor? {
    switch color {
    case .abpAlmostWhite:
        return UIColor(named: "abpAlmostWhite")
    case .abpBackgroundGray:
        return UIColor(named: "abpBackgroundGray")
    case .abpDarkGray:
        return UIColor(named: "abpDarkGray")
    case .abpLightGray:
        return UIColor(named: "abpLightGray")
    case .abpRed:
        return UIColor(named: "abpRed")
    case .abpPureWhite:
        return UIColor(named: "abpPureWhite")
    case .abpPureWhiteTrans:
        return UIColor(named: "abpPureWhiteTrans")
        }
    }
}
