/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import LinkPresentation
import MobileCoreServices
import SafariServices
import UIKit

class ActionViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var urlTitleLabel: UILabel!
    @IBOutlet weak var urlDetailLabel: UILabel!
    @IBOutlet weak var urlInfoView: UIView!
    @IBOutlet weak var favIconView: UIView!
    @IBOutlet weak var favIconImageView: UIImageView!
    @IBOutlet weak var favIconLabel: UILabel!

    var hostnameString: String = ""
    var activeURL = URL(string: "")

    // MARK: - IBActions

    @IBAction func addButtonPressed() {
        setLoadingState(asEnabled: true)
        let dataSource = WhitelistItemDataSource()

        // Add URL to whitelist
        dataSource.appendToWhitelist(hostname: hostnameString) { rules in
            ContentBlockerManager.shared().reloadContentBlocker()
        }
    }

    @IBAction func cancelButtonPressed() {
        // Return any edited content to the host app.
        // This template doesn't do anything, so we just echo the passed in items.
        self.extensionContext!.completeRequest(returningItems: self.extensionContext!.inputItems, completionHandler: nil)
    }

    // MARK: - View Logic

    override func viewDidLoad() {
        super.viewDidLoad()
        getActiveURL()
        ContentBlockerManager.shared().delegate = self
    }

    // Apply custom colors when app switches between dark mode.
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.urlInfoView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
        self.favIconView.layer.borderColor = UIColor.customColor(.abpAlmostWhite)?.cgColor
    }

    /// Starts animation spinner and disables UIButtons
    /// - Parameter state: Passing true will enable loading state or false will disable the loading state.
    fileprivate func setLoadingState(asEnabled state: Bool) {
        DispatchQueue.main.async {
            self.addButton.isEnabled = !state
            self.cancelButton.isEnabled = !state
            switch state {
            case true:
                self.activityIndicator.startAnimating()
            default:
                self.activityIndicator.stopAnimating()
            }
        }
    }

    // MARK: - Retreve URL + Metadata

    fileprivate func getActiveURL() {
        if let inputItem = extensionContext?.inputItems.first as? NSExtensionItem {
            if let itemProvider = inputItem.attachments?.first {
                itemProvider.loadItem(forTypeIdentifier: kUTTypePropertyList as String) { plist, _ in
                    guard let dictionary = plist as? NSDictionary else { return }
                    if let dictionary2 = dictionary[NSExtensionJavaScriptPreprocessingResultsKey] as? NSDictionary {
                        guard let pageTitle = dictionary2["title"] as? String,
                            let pageURL = dictionary2["url"] as? String,
                            let pageHostname = NSString(string: pageURL ).whitelistedHostname() else {return}
                        let firstLetterOfHostname = pageHostname.prefix(1).capitalized
                        self.hostnameString = pageHostname
                        DispatchQueue.main.async {
                            self.urlTitleLabel.text = pageTitle
                            self.urlDetailLabel.text = pageHostname
                            self.favIconLabel.text = firstLetterOfHostname
                            if #available(iOSApplicationExtension 13.0, *) {
                                self.applyLinkMetadataToUI()
                            }
                        }
                    }
                }
            }
        }
    }

    /// Retrieves and applies URL metadata to the UI
    @available(iOSApplicationExtension 13.0, *)
    fileprivate func applyLinkMetadataToUI() {
        let metadataProvider = LPMetadataProvider()
        // Pass URL to metadata provider
        DispatchQueue.main.async {
            metadataProvider.startFetchingMetadata(for: URL(string: "http://\(self.hostnameString)")!) { metadata, error in
                if error != nil { return }

                // Update UI on main queue
                DispatchQueue.main.async {
                    self.urlTitleLabel.text = metadata?.title
                }

                if metadata?.iconProvider?.hasItemConformingToTypeIdentifier(kUTTypeImage as String) == true {
                    metadata?.iconProvider?.loadItem(forTypeIdentifier: kUTTypeImage as String, options: nil) { item, error in
                        if error != nil { return }
                        guard let imageData = item as? Data,
                            let image = UIImage(data: imageData) else { return }
                        self.setFaviconImage(image)
                    }
                } else if metadata?.imageProvider?.hasItemConformingToTypeIdentifier(kUTTypeImage as String) == true {
                    metadata?.imageProvider?.loadItem(forTypeIdentifier: kUTTypeImage as String, options: nil) { item, error in
                        if error != nil { return }
                        guard let imageData = item as? Data,
                            let image = UIImage(data: imageData) else { return }
                        self.setFaviconImage(image)
                    }
                }
            }
        }
    }

    fileprivate func setFaviconImage(_ image: UIImage) {
        // Update UI on main queue
        DispatchQueue.main.async {
            self.favIconImageView.image = image
            // Sometimes favicons have transparancy, so we reset the label text and background color.
            self.favIconLabel.text = ""
            self.favIconView.backgroundColor = .white
        }
    }
}

// MARK: - Delegate Extension

extension ActionViewController: ContentBlockerManagerDelegate {
    func contentBlockerStateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
        self.extensionContext!.completeRequest(returningItems: self.extensionContext!.inputItems, completionHandler: nil)
    }

    func contentBlockerUpdateDidFail(error: Error) {
        setLoadingState(asEnabled: false)
        self.extensionContext!.completeRequest(returningItems: self.extensionContext!.inputItems, completionHandler: nil)
    }

    func contentBlockerUpdateDidSucceed() {
        setLoadingState(asEnabled: false)
        self.extensionContext!.completeRequest(returningItems: self.extensionContext!.inputItems, completionHandler: nil)
    }
}
