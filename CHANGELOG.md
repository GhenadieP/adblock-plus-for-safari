# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2020-02-06
### Changed
* Updated bundled filter lists

## [2.0.0] - 2019-12-11
### Added
App rewritten from scratch with the following features:
* A Redesign of the whole application
* Support for Dark Mode (iOS 13 only)
* User controlled domain whitelisting
* Acceptable Ads configuration toggle
* Opt-in Firebase Analyictics and crash reporting
* Safari Action Extension for whitelisting